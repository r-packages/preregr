Dear CRAN maintainers,

The preregistration form for systematic reviews has been updated to the version on OSF.

Thank you very much for your work on CRAN!

Kind regards,

Gjalt-Jorn
